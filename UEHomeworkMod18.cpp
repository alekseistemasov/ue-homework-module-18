// UEHomeworkMod18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Player {
private:
    std::string name;
    int score;
public:
    Player() {
        name = "";
        score = 0;
    }
    Player(std::string name, int score) {
        this->name = name;
        this->score = score;
    }
    std::string GetName() {
        return name;
    }
    int GetScore() {
        return score;
    }
};


void quicksortDescending(Player* l, Player* r) {
    Player z = *(l + (r - l) / 2);
    Player* ll = l, *rr = r;
    while (ll <= rr) {
        while (ll->GetScore() > z.GetScore()) ll++;
        while (rr->GetScore() < z.GetScore()) rr--;
        if (ll <= rr) {
            std::swap(*ll, *rr);
            ll++;
            rr--;
        }
    }
    if (l < rr) quicksortDescending(l, rr + 1);
    if (ll < r) quicksortDescending(ll, r);
}

void printPlayerArray(Player* array, int arrayCount) {
    for (int i = 0; i < arrayCount; i++) {
        std::cout << "player " << i << " " << "name: " << array[i].GetName() << ", ";
        std::cout << "score: " << array[i].GetScore() << "\n";
    }

}

int main()
{
    int playersQuantity;
    std::cout << "enter number of players" << "\n";
    std::cin >> playersQuantity;
    Player* players = new Player[playersQuantity];
    for (int i = 0; i < playersQuantity; i++) {
        std::string name;
        int score;
        std::cout << "enter name of player " << i << "\n";
        std::cin >> name;
        std::cout << "enter score of player " << i << "\n";
        std::cin >> score;
        players[i] = Player(name, score);
    }

    std::cout << "\n";
    printPlayerArray(players, playersQuantity);
    std::cout << "\n";
    std::cout << "sorted players: \n";
    quicksortDescending(players, players + (playersQuantity - 1));
    printPlayerArray(players, playersQuantity);
    delete [] players;

    

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
